package AverageMin;

public class BankAccount implements Measurable {
	
	private String accountname;
	private double balance;

	public BankAccount(String accountname,double balance) {
		this.accountname = accountname;
		this.balance = balance;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return balance;
	}
	public String getaccountName() {  
		return accountname; 
	}
	public double getBalance() {  
		return balance; 
	}

}
