package AverageMin;

public class Country implements Measurable {
	private String country;
	private double area;

	public Country(String country, double area) {
		this.country = country;
		this.area = area;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return area;
	}
	
	public String getNamecountry() {
		return country;
	}

}
