package AverageMin;

public class MinTestCase {
	
	public static void main (String args[]){
		
		Data data = new Data();
		//---------Person---------
		Measurable[] person_data = new Measurable[5];
		person_data[0] = new Person("Fon",158);
		person_data[1] = new Person("Air",157);
		person_data[2] = new Person("Yomost",170);
		person_data[3] = new Person("Chertam",165);
		person_data[4] = new Person("Mochi",162);
		
		//--------BankAcount-------
		Measurable[] bank_data = new Measurable[5];
		bank_data[0] = new BankAccount("Bank1",500);
		bank_data[1] = new BankAccount("Bank2",1200);
		bank_data[2] = new BankAccount("Bank3",800);
		bank_data[3] = new BankAccount("Bank4",2000);
		bank_data[4] = new BankAccount("Bank5",350);
		
		//------Country----------
		
		Measurable[] country_data = new Measurable[5];
		country_data[0] = new Country("Thailand",513120);
		country_data[1] = new Country("Korea",309200);
		country_data[2] = new Country("America",980300);
		country_data[3] = new Country("France",780300);
		country_data[4] = new Country("Laos",239000);
		
		//---------AverageTest-----
		System.out.println("---AverageTest---");
		double averageHeight = Data.average(person_data);
		System.out.println("Average height : "+averageHeight);
		
		double averageBalance = Data.average(bank_data);
		System.out.println("Average Balance : "+averageBalance);
		
		double averageArea = Data.average(country_data);
		System.out.println("Average Area : "+averageArea);
		
		System.out.println("__________________________________________________________________");
		//-----MinTest----
		
		System.out.println("---MinTest---");
		
		Country thailand = new Country("Thailand",513120);
		Country france = new Country("France",780300);
		Measurable smaller = Data.min(thailand,france);
		Country smallerCountry = (Country)smaller;
		System.out.println("Smaller country between Thailand and France is : "+ smallerCountry.getNamecountry());
		
		Person chertam = new Person("Chertam",165);
		Person air = new Person("Air",157);
		Measurable shorter = Data.min(chertam, air);
		Person shorterPerson = (Person)shorter;
		System.out.println("Shorter person  between Air and Chertam is : "+ shorterPerson.getName());
		
		BankAccount bank1 = new BankAccount("Bank1",84000);
		BankAccount bank2 = new BankAccount("Bank2",45800);
		Measurable less = Data.min(bank1, bank2);
		BankAccount lessBalance = (BankAccount)less;
		System.out.println("Accounts with balances is : "+ lessBalance.getaccountName());
		
		
	
		
			
		
	}
}
