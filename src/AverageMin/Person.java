package AverageMin;

public class Person implements Measurable {
	private String name ; 
	private double height;

	public Person(String name,double height) {
		
		this.name = name;
		this.height = height;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		
		return height;
	}
	
	public String getName() {
		return name;
	}
}
