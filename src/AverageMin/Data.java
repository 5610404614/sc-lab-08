package AverageMin;

public class Data {
	
	
	public static double average(Measurable[] meas){
		double sum = 0;
		for (Measurable obj : meas){
			
			sum += obj.getMeasure();
			
		}
		
		if (meas.length>0){
			return sum/meas.length;
			
		}
		else {
			return 0;
		}
		
	}
	
	public static Measurable min(Measurable m1,Measurable m2){
		if(m1.getMeasure()<m2.getMeasure()){
			return m1;
		}
		else {
			return m2;
		}
		
	}
	
	
	
}
