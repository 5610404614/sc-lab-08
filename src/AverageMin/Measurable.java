package AverageMin;

public interface Measurable {
	
	double getMeasure();

}
