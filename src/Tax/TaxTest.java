package Tax;

import java.util.ArrayList;

public class TaxTest {
	public static void main(String args[]){
		TaxCalculator taxcal = new TaxCalculator();
		//---------Person----------
		System.out.println("<<Person>>");
		ArrayList<Taxable> person_data = new ArrayList<Taxable>();
		Person person1 = new Person("Joe",100000);
		Person person2 = new Person("Annie",500000);
		person_data.add(person1);
		person_data.add(person2);
		System.out.println("Sum tax of person is : " + taxcal.sum(person_data));
		
		
		
		//---------Company---------
		System.out.println("<<Company>>");
		ArrayList<Taxable> company_data = new ArrayList<Taxable>();
		Company company1= new Company("Univesal",1000000,800000);
		Company company2 = new Company("Walt Disney",5000000,2000000);
		company_data.add(company1);
		company_data.add(company2);
		System.out.println("Sum tax of Company is : " + taxcal.sum(company_data));
		
		//--------Product----------

		System.out.println("<<Product>>");
		ArrayList<Taxable> product_data = new ArrayList<Taxable>();
		Product product1= new Product("Macbook",45000);
		Product product2 = new Product("Powerbank",500);
		product_data.add(product1);
		product_data.add(product2);
		System.out.println("Sum tax of Company is : " + taxcal.sum(product_data));
				
		
		//--------Product----------

		System.out.println("<<Person&Company&Product>>");
		ArrayList<Taxable> mix_data = new ArrayList<Taxable>();
		Person person = new Person("Joe",100000);
		Company company= new Company("Univesal",1000000,800000);
		Product product = new Product("Powerbank",500);
		mix_data.add(person);
		mix_data.add(company);
		mix_data.add(product);
		System.out.println("Sum tax of Company is : " + taxcal.sum(mix_data));
						
	
		
	}

}
