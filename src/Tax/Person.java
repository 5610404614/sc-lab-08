package Tax;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Person implements Taxable {
	
	private ArrayList<Person> person_data ;
	private String name;
	private double income;
	
	

	public Person(String name, double income) {
		this.name = name;
		this.income = income;
	}


	@Override
	public double getTax() {
		if (income <= 300000 && income>=0){
			
			return income*(0.05);
			
		}
	
		return 15000 + (0.1)*(income-300000);
			
		
		
		
	}

}
