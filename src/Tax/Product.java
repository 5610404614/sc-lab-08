package Tax;

public class Product implements Taxable {
	private String nameProduct;
	private double price;

	public Product(String name, double price) {
		this.nameProduct = name;
		this.price = price;
	}

	@Override
	public double getTax() {
		
		return (price*7)/100;
	}

}
