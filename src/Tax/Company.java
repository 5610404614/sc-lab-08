package Tax;

public class Company implements Taxable{
	
	private String nameCompany ;
	private double income;
	private double outcome;


	public Company(String name, double in, double out) {
		this.nameCompany = name;
		this.income = in;
		this.outcome = out;
		
	}


	@Override
	public double getTax() {
		
		return (income-outcome)*(0.3);
	}

}
